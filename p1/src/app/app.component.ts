import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

//model -data
export class AppComponent {
  title = "James Wilson's blog";
  today = new Date();
  notes = [
    { desc: 'What is GMT', date: new Date() },
    { desc: 'Will i play WOW', date: new Date() },
    { desc: 'am i BATMAN', date: new Date() }
  ]

   flag = true;
   showFlag = true;
   otherID = "billgates"
   myUrl = "http://via.placeholder.com/150";
  //  myUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/CarrotTop.jpg/800px-CarrotTop.jpg";

  handleClick() {
    
    this.flag=false;
    this.showFlag = true;
 
    console.log(Event)
  }

  handleCustomEvent(event){
    // console.log(event.name)
    console.log(event)
  
  }
}