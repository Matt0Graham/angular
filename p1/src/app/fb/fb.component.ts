import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//import { parseHttpResponse, HttpClient } from 'selenium-webdriver/http';

@Component({
  selector: 'app-fb',
  templateUrl: './fb.component.html',
  styleUrls: ['./fb.component.css']
})
export class FbComponent implements OnInit {
  
  @Input () id = "mickeymouse";
  fbIdUrl = 'https://graph.facebook.com/mickeymouse/picture?type=normal';

  @Output () myEvent = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }


  resolveImageUrl(event){   
    this.fbIdUrl = `https://graph.facebook.com/${this.id}/picture?type=normal`

    this.myEvent.emit({type:"big",name:"james is a noot noot"});

    
  }

}
